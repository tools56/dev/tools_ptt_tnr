FROM ubuntu:18.04
ENV TZ Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

LABEL version="1.0"
LABEL description="Environnement de base pour execution des tests PTT et Protractor"

RUN apt-get update

# Composant pour docker
RUN apt-get install -y apt-transport-https
RUN apt-get install -y ca-certificates 
RUN apt-get install -y software-properties-common
RUN apt-get install -y curl

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
RUN apt-get update
#RUN apt-get install -y docker-ce
RUN apt-get install -y docker-ce-cli

# Composant pour openjdk"
RUN apt-get install -y openjdk-8-jdk
RUN apt-get install -y openjdk-8-jre

# Composant pour git
RUN apt-get install -y git

# Composant pour Apparmor: gestion des droits d'accès sur les applications
RUN apt-get install -y apparmor

# Composant pour vim-nox : Editeur vi amélioré
RUN apt install -y vim-nox
#vim-nox install zip - ruby

# Composant pour Oracle
#retrieves files from the web
RUN apt-get install -y wget
#convert and install rpm and other packages
RUN apt-get install -y alien

RUN wget -P ~/Oracle/ https://download.oracle.com/otn_software/linux/instantclient/193000/oracle-instantclient19.3-basic-19.3.0.0.0-1.x86_64.rpm
RUN wget -P ~/Oracle/ https://download.oracle.com/otn_software/linux/instantclient/193000/oracle-instantclient19.3-devel-19.3.0.0.0-1.x86_64.rpm
RUN wget -P ~/Oracle/ https://download.oracle.com/otn_software/linux/instantclient/193000/oracle-instantclient19.3-sqlplus-19.3.0.0.0-1.x86_64.rpm

RUN alien -i ~/Oracle/oracle-instantclient19.3-*.rpm
RUN apt-get install -y libaio1
RUN apt-get install -y libaio-dev

# Composant pour Python2
RUN apt-get install -y python
#error-tolerant HTML parser for Python
RUN apt-get install -y python-bs4                                      
#universal character encoding detector for Python2
RUN apt-get install -y python-chardet 
#pythonic binding for the libxml2 and libxslt libraries
RUN apt-get install -y python-lxml    
                                                                                                                  
# Composant pour Python3                           
RUN apt-get install -y python3
RUN apt-get install -y python3-pkg-resources                           
RUN apt-get install -y python3-requests 
RUN apt-get install -y python3-urllib3

# Composant pour Python 2 et 3 
#Python interface to libapt-pkg (locales)                                          
RUN apt-get install -y python-apt-common                                                                    
#HTML parser/tokenizer based on the WHATWG HTML5 specification                            
RUN apt-get install -y python-html5lib 

#A partir de là a controler
#classical file transfer client
#RUN apt-get install -y ftp
#fast, versatile, remote (and local) file-copying tool
RUN apt-get install -y rsync
#Light-weight package to set up cgroupfs mounts
RUN apt-get install -y cgroupfs-mount
#Tools to manage aufs filesystems
RUN apt-get install -y aufs-tools
#simple configuration storage system - D-Bus service
RUN apt-get install -y dconf-service
#Fonts with the same metrics as Times, Arial and Courier
RUN apt-get install -y fonts-liberation
#GNU privacy guard - cryptographic agent (dummy transitional package)
RUN apt-get install -y gnupg-agent
#GNU privacy guard - a free PGP replacement (dummy transitional package)
RUN apt-get install -y gnupg2
#GSettings desktop-wide schemas
RUN apt-get install -y gsettings-desktop-schemas
#NET-3 networking toolkit
RUN apt-get install -y net-tools 
#additional terminal type definitions
RUN apt-get install -y ncurses-term
#Dispatcher service for systemd-networkd connection status changes
RUN apt-get install -y networkd-dispatcher
#GNU bc arbitrary precision calculator language
RUN apt-get install -y bc
#network-related giomodules for GLib
RUN apt-get install -y glib-networking
#system and service manager - SysV links
RUN apt-get install -y systemd-sysv 
#displays an indented directory tree, in color
RUN apt-get install -y tree
#Non-interactive ssh password authentication
RUN apt-get install -y sshpass 
#secure shell (SSH) server, for secure access from remote machines
RUN apt-get install -y openssh-server
#securely retrieve an SSH public key and install it locally
RUN apt-get install -y ssh-import-id  
#runtime components for the Universally Unique ID library	
RUN apt-get install -y uuid-runtime
#X server utilities                               
#RUN apt-get install -y x11-xserver-utils
#desktop integration utilities from freedesktop.org
#RUN apt-get install -y xdg-utils                                       
#X Keyboard Extension (XKB) configuration data
#RUN apt-get install -y xkb-data

#lib non installée
#?? libappindicator3-1
#google-chrome-stable
